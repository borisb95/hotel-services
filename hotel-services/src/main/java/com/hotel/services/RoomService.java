package com.hotel.services;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hotel.beans.Reservation;
import com.hotel.beans.Room;
import com.hotel.controllers.RoomController;
import com.hotel.repositories.ReservationRepository;
import com.hotel.repositories.RoomRepository;

@Service
public class RoomService {
	private static final Logger LOGGER = LoggerFactory.getLogger(RoomController.class);
	
	@Autowired
	RoomRepository roomRepository;
	
	@Autowired
	ReservationRepository reservationRepository;
	
	public TreeSet<Room> getAvailableRoomsInAllHotels(LocalDate startDate, 
			LocalDate endDate, int numPerson) {
		TreeSet<Room> allAvailableRooms = new TreeSet<>(comparatorByPriceName());
		
		List<Room> roomsWithThatCapacity = roomRepository.findByMaxCapacity(numPerson);
		//Adding the available rooms
		for (Room room : roomsWithThatCapacity) {
			
			boolean isAvailable = true;
			for (String resId : room.getReservationIds()) {
				Reservation res = reservationRepository.findById(resId).get();
//				if(res.getEnd().isBefore(startDate))
//					continue;
				LOGGER.info("{} begin date {}, end date {}", room.getName(),res.getBegin(), res.getEnd());
				LOGGER.info("RE begin date {}, end date {}", startDate, endDate);
				LOGGER.info("||||||||||||||||||||||||");

				if((startDate.isAfter(res.getBegin()) && startDate.isBefore(res.getEnd())) ||
						startDate.isEqual(res.getBegin()) ||
						endDate.isEqual(res.getEnd()) ||
						(endDate.isAfter(res.getBegin()) && endDate.isBefore(res.getEnd())) ||
						(startDate.isBefore(res.getBegin()) && endDate.isAfter(res.getEnd()))) {
					isAvailable = false;
					break;
				}	
			} 
			if(isAvailable)
				allAvailableRooms.add(room);
		}
		
		
		return allAvailableRooms;
	}
	
	private Comparator<Room> comparatorByPriceName(){
		return (r1, r2) ->{
			int price1 = r1.getPrice().getPrice();
			int price2 = r2.getPrice().getPrice();
			
			if(price1 - price2 == 0) {
				return r1.getName().compareTo(r2.getName());
			}
			return price1 - price2;
		};
	}

	public Room updateMainInfo(Room room) {
		Room oldRoom = roomRepository.findById(room.getId()).get();
		oldRoom.setName(room.getName());
		oldRoom.setNormalCapacity(room.getNormalCapacity());
		oldRoom.setMaxCapacity(room.getMaxCapacity());
		Room changedRoom = roomRepository.save(oldRoom);
		return changedRoom;
	}
}
