package com.hotel.services;

import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hotel.beans.Reservation;
import com.hotel.beans.Room;
import com.hotel.repositories.ReservationRepository;
import com.hotel.repositories.RoomRepository;

@Service
public class ReservationService {
	
	@Autowired
	RoomRepository roomRepository;
	
	@Autowired
	ReservationRepository reservationRepository;
	
	public void getAllReservations(Map<String, TreeSet<Reservation>> allReservations, String hotelId) {
		List<Room> allHotelRooms = roomRepository.findByHotelIdEquals(hotelId);
		
		for (Room room : allHotelRooms) {
			String roomId = room.getId();
			allReservations.put(roomId, new TreeSet<>());
			for (String resId : room.getReservationIds()) {
				Reservation res = reservationRepository.findById(resId).get();
				allReservations.get(roomId).add(res);
			}
		}
	}
	
	public Reservation updateReservation(Reservation reservation) {
		Reservation updatedReservation = reservationRepository.save(reservation);
		return updatedReservation;
	}
	
	public Reservation createReservation(Reservation reservation,
			String roomId){
		Reservation createdReservation = reservationRepository.save(reservation);
		Room room = roomRepository.findById(roomId).get();
		room.addReservation(createdReservation.getId());
		roomRepository.save(room);
		
		return createdReservation;
	}
	
	public void deleteReservation(Reservation reservation) {
		this.reservationRepository.delete(reservation);
	}
}
