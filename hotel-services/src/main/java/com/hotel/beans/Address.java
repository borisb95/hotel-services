package com.hotel.beans;

import org.springframework.data.annotation.Id;

public class Address {
	@Id
	private String id;
    private String city;
    private String country;
    
    public Address() {}

    public Address(String city, String country){
    	this.city = city;
    	this.country = country;
    }

	public String getId() {
		return id;
	}

	public String getCity() {
		return city;
	}

	public String getCountry() {
		return country;
	}
}
