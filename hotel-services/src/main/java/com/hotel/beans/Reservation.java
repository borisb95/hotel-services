package com.hotel.beans;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Reservations")
public class Reservation implements Comparable<Reservation>{
	@Id
	private String id;
	@NotNull
	private LocalDate begin;
	@NotNull
	private LocalDate end;
	private Client client;

	private Reservation() {
	}

	public String getId() {
		return id;
	}

	public LocalDate getBegin() {
		return begin;
	}

	public LocalDate getEnd() {
		return end;
	}

	public Client getClient() {
		return client;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((begin == null) ? 0 : begin.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Reservation other = (Reservation) obj;
		if (begin == null) {
			if (other.begin != null)
				return false;
		} else if (!begin.equals(other.begin))
			return false;
		return true;
	}

	@Override
	public int compareTo(Reservation o) {
		return this.begin.compareTo(o.getBegin());
	}
}
