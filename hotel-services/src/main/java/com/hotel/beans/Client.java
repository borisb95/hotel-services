package com.hotel.beans;

import org.springframework.data.annotation.Id;

public class Client {
	@Id
	private String id;
	private String name;
	private String egn;
	private String address;
	private String mvr;

	private Client() {
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEgn() {
		return egn;
	}

	public String getAddress() {
		return address;
	}

	public String getMvr() {
		return mvr;
	}

}
