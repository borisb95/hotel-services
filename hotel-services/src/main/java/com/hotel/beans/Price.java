package com.hotel.beans;

import org.springframework.data.annotation.Id;

public class Price {
	@Id
	private String id;
	private int price;
	private int addPerson;
	
	protected Price() {}
	
	public Price(int price, int addPerson) {
		super();
		this.price = price;
		this.addPerson = addPerson;
	}

	public String getId() {
		return id;
	}

	public int getPrice() {
		return price;
	}

	public int getAddPerson() {
		return addPerson;
	}
	
	
}
