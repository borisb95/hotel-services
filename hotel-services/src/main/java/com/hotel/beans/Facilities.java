package com.hotel.beans;

import org.springframework.data.annotation.Id;

public class Facilities {
	@Id
	private String id;
	private boolean airCond;
	private boolean wifi;
	private boolean tv;
	private boolean privateBath;
	
	public Facilities(boolean airCond, boolean wifi, boolean tv, boolean privateBath) {
		this.airCond = airCond;
		this.wifi = wifi;
		this.tv = tv;
		this.privateBath = privateBath;
	}
	
	protected Facilities() {}

	public String getId() {
		return id;
	}

	public boolean isAirCond() {
		return airCond;
	}

	public boolean isWifi() {
		return wifi;
	}

	public boolean isTv() {
		return tv;
	}

	public boolean isPrivateBath() {
		return privateBath;
	}
	
}
