package com.hotel.beans;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Range;
import org.springframework.data.annotation.Id;

public class Furniture {
	@Id
	private String id;
	@Range(min=0, max=20, message = "Ne moje takova chislo bace")
	private int singleBed;
	@Range(min=0, max=10, message = "Ne moje takova chislo bace")
	private int doubleBed;
	private int sofa;
	private int extSofa;
	
	protected Furniture() {}
	
	public Furniture(int singleBed, int doubleBed, int sofa, int extSofa) {
		this.singleBed = singleBed;
		this.doubleBed = doubleBed;
		this.sofa = sofa;
		this.extSofa = extSofa;
	}

	public String getId() {
		return id;
	}

	public int getSingleBed() {
		return singleBed;
	}

	public int getDoubleBed() {
		return doubleBed;
	}

	public int getSofa() {
		return sofa;
	}

	public int getExtSofa() {
		return extSofa;
	}
	
	
	
}
