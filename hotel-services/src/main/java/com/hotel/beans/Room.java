package com.hotel.beans;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Rooms")
public class Room implements Comparable<Room> {
	@Id
	private String id;
	@NotNull
	private String hotelId;
	private Set<String> reservationIds;
	@NotNull
	private String name;
	private int normalCapacity;
	private int maxCapacity;
	private Furniture furniture;
	private Facilities facilities;
	private Price price;

	private Room() {
		this.reservationIds = new HashSet<>();
	}

	public Room(String hotelId, String name, int normalCapacity, int maxCapacity, Furniture furniture,
			Facilities facilities, Price price) {
		this.hotelId = hotelId;
		this.reservationIds = new HashSet<>();
		this.name = name;
		this.normalCapacity = normalCapacity;
		this.maxCapacity = maxCapacity;
		this.furniture = furniture;
		this.facilities = facilities;
		this.price = price;
	}

	public Furniture addFurniture(Furniture furniture) {
		this.furniture = furniture;
		return this.furniture;
	}

	public Room addReservation(String reservationId) {
		this.reservationIds.add(reservationId);
		return this;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public void setNormalCapacity(int normalCapacity) {
		this.normalCapacity = normalCapacity;
	}

	public void setMaxCapacity(int maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	public int getNormalCapacity() {
		return normalCapacity;
	}

	public int getMaxCapacity() {
		return maxCapacity;
	}

	public Set<String> getReservationIds() {
		return reservationIds;
	}

	public String getHotelId() {
		return hotelId;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Furniture getFurniture() {
		return furniture;
	}

	public Facilities getFacilities() {
		return facilities;
	}

	public Price getPrice() {
		return price;
	}

	@Override
	public int compareTo(Room o) {
		return this.name.compareTo(o.getName());
	}
}
