package com.hotel.exception;

import java.util.List;

import org.springframework.http.HttpStatus;

public class ExceptionResponse {
	private int errorCode;
	private String errorMessage;
	private List<String> errors;
	
	
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public List<String> getErrors() {
		return errors;
	}	
}
