package com.hotel.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.hotel.beans.Room;

public interface RoomRepository extends MongoRepository<Room, String> {
	List<Room> findByHotelIdEquals(String hotelId);
	
//	@Query(value = "{'$and' : [{'maxCapacity': { $lte:?0 }}, {}]}")
	@Query(value = "{'maxCapacity': { '$gte':?0 }}")
	List<Room> findByMaxCapacity(int guests);
}
