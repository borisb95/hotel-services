package com.hotel.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.hotel.beans.Hotel;

@Repository
public interface HotelRepository extends MongoRepository<Hotel, String>{
	List<Hotel> findByNameContaining(String a);
}
