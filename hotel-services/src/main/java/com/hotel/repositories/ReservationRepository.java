package com.hotel.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hotel.beans.Reservation;

public interface ReservationRepository extends MongoRepository<Reservation, String>{

}
