package com.hotel.controllers;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.TreeSet;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hotel.beans.Furniture;
import com.hotel.beans.Room;
import com.hotel.repositories.HotelRepository;
import com.hotel.repositories.RoomRepository;
import com.hotel.services.RoomService;

@RestController
@RequestMapping("/rooms")
public class RoomController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoomController.class);

	@Autowired
	RoomRepository roomRepository;
	@Autowired
	HotelRepository hotelRepository;
	@Autowired
	RoomService roomService;

	@GetMapping("/allRooms")
	public List<Room> getAllRooms(@RequestParam(value = "hotelId", required = true) String hotelId) {
		List<Room> allRooms = roomRepository.findByHotelIdEquals(hotelId);

		return allRooms;
	}

	@GetMapping("/availableRoomsAllHotels")
	public ResponseEntity<TreeSet<Room>> getAvailableRoomsInAllHotels(
			@RequestParam(value="startDate", required=true) @DateTimeFormat(iso = ISO.DATE) LocalDate startDate,
			@RequestParam(value="endDate", required=true)  @DateTimeFormat(iso = ISO.DATE) LocalDate endDate,
			@RequestParam(value="numPerson", required=true) @Min(value = 1, message = "The value must be positive") Integer numPerson){
		TreeSet<Room> availableRooms = roomService.getAvailableRoomsInAllHotels(startDate,
				endDate, numPerson);
		
		return ResponseEntity.ok(availableRooms);
	}

	@GetMapping("/{id}")
	public Optional<Room> getRoom(@PathVariable("id") String roomId) {
		Optional<Room> room = roomRepository.findById(roomId);

		return room;
	}

	@PostMapping("/create")
	public ResponseEntity<Room> createRoom(@RequestBody Room room) {
		Room savedRoom = roomRepository.save(room);

		return ResponseEntity.ok(savedRoom);
	}
	
	@PutMapping("/updateMainInfo")
	public ResponseEntity<Room> updateMainInfo(@RequestBody Room room){
		Room changedRoom = roomService.updateMainInfo(room);
		
		return ResponseEntity.ok(changedRoom);
	}

	@PutMapping("/updateFurniture")
	public ResponseEntity<Furniture> updateFurniture(@Valid @RequestBody Furniture furniture,
			@RequestParam(value = "roomId", required = true) String roomId) {
		Room room = roomRepository.findById(roomId).get();
		Furniture updatedFurn = room.addFurniture(furniture);

		roomRepository.save(room);

		return ResponseEntity.ok(updatedFurn);
	}

	@DeleteMapping("/delete/{id}")
	public void deleteRoom(@PathVariable("id") String roomId) {
		roomRepository.deleteById(roomId);
	}
}
