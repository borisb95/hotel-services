package com.hotel.controllers;

import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hotel.beans.Reservation;
import com.hotel.services.ReservationService;

@RestController
@RequestMapping("/reservations")
public class ReservationController {

	@Autowired
	ReservationService reservationService;

	@GetMapping("/getAll")
	public Map<String, TreeSet<Reservation>> getAllReservationsFromHotel(
			@RequestParam(value = "hotelId", required = true) String hotelId) {
		Map<String, TreeSet<Reservation>> allReservations = new TreeMap<>();
		reservationService.getAllReservations(allReservations, hotelId);

		return allReservations;
	}

	@PutMapping("/update")
	public Reservation updateReservation(@RequestBody Reservation reservation) {
		Reservation updatedReservation = reservationService.updateReservation(reservation);
		return updatedReservation;
	}

	@PostMapping("/create")
	public ResponseEntity<Reservation> createReservation(@RequestBody Reservation reservation,
			@RequestParam(value = "roomId", required = true) String roomId) {
		Reservation createdReservation = reservationService.createReservation(reservation, roomId);

		return ResponseEntity.ok(createdReservation);
	}

	@DeleteMapping("/delete")
	public void deleteReservation(@RequestParam(value = "reservationId", required = true) Reservation reservation) {
		reservationService.deleteReservation(reservation);
	}
}
