package com.hotel.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hotel.beans.Hotel;
import com.hotel.repositories.HotelRepository;

@RestController
@RequestMapping("/hotels")
public class HotelController {
	@Autowired
	private HotelRepository hotelRepository;
	
	
	@GetMapping("/id/{id}")
	public Optional<Hotel> getMyHotelById(@PathVariable("id") String id) {
		Optional<Hotel> myHotel = hotelRepository.findById(id);
		
		return myHotel;
	}
	
	@GetMapping("/all")
	public List<Hotel> getAllHotels(@PathVariable("hotelId") String hotelId){
		List<Hotel> allHotels = hotelRepository.findAll();
		
		return allHotels;
	}
	
	@GetMapping("/containing/{cont}")
	public List<Hotel> getHotelContaining(@PathVariable("cont") String cont){
		List<Hotel> hotels = hotelRepository.findByNameContaining(cont);
		
		return hotels;
	}
	
	@PutMapping("/update")
	public ResponseEntity<Hotel> updateHotel(@RequestBody Hotel hotel){
		Hotel updatedHotel = hotelRepository.findById(hotel.getId()).get();
		//Do the update
		
		return ResponseEntity.ok(updatedHotel);
	}
	
	@PostMapping("/create")
	public ResponseEntity<Hotel> createHotel(@RequestBody Hotel hotel) {
		Hotel createdHotel = this.hotelRepository.save(hotel);
		
		return ResponseEntity.ok(createdHotel);
	}
	
	@DeleteMapping("/{id}")
	public void deleteHotel(@PathVariable("id") String id) {
		this.hotelRepository.deleteById(id);
	}
}
